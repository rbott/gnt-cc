package main

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

type rapiJobStatusCount struct {
	Canceled int `json:"canceled"`
	Error    int `json:"error"`
	Pending  int `json:"pending"`
	Queued   int `json:"queued"`
	Success  int `json:"success"`
	Waiting  int `json:"waiting"`
}

type createInstanceParameters struct {
	InstanceName      string       `json:"instance_name"`
	DiskTemplate      string       `json:"disk_template"`
	Vcpus             int          `json:"vcpus,omitempty"`
	MemoryInMegabytes int          `json:"memoryInMegabytes,omitempty"`
	Nics              []ganetiNic  `json:"nics,omitempty"`
	Disks             []ganetiDisk `json:"disks,omitempty"`
}

type createMultiInstancesParameters []createInstanceParameters

func getclustersHandler(c *gin.Context) {
	var names []string
	for _, cluster := range C.Clusters {
		names = append(names, cluster.Name)
	}
	c.JSON(200, gin.H{
		"clusters": names,
	})
}

func getClusterHandler(c *gin.Context) {
	name := c.Param("cluster")
	if !isValidCluster(name) {
		c.JSON(404, gin.H{"code": "PAGE_NOT_FOUND", "message": "Cluster not found"})
	} else {
		c.JSON(200, gin.H{
			"cluster": name,
		})
	}
}

func getNodesHandler(c *gin.Context) {
	name := c.Param("cluster")
	if !isValidCluster(name) {
		c.JSON(404, gin.H{"code": "PAGE_NOT_FOUND", "message": "Cluster not found"})
	} else {
		content, err := rapiGet(name, "/2/nodes?bulk=1")
		if err != nil {
			c.JSON(502, gin.H{
				"code":    "INTERNAL_SERVER_ERROR",
				"message": fmt.Sprintf("RAPI Backend Error: %s", err),
			})
			return
		}
		var nodesData rapiNodesBulk
		json.Unmarshal([]byte(content), &nodesData)
		c.JSON(200, gin.H{
			"cluster":       name,
			"numberOfNodes": len(nodesData),
			"nodes":         nodesData,
		})
	}
}

func getInstancesHandler(c *gin.Context) {
	name := c.Param("cluster")
	if !isValidCluster(name) {
		c.JSON(404, gin.H{"code": "PAGE_NOT_FOUND", "message": "Cluster not found"})
	} else {
		content, err := rapiGet(name, "/2/instances?bulk=1")
		if err != nil {
			c.JSON(502, gin.H{
				"code":    "INTERNAL_SERVER_ERROR",
				"message": fmt.Sprintf("RAPI Backend Error: %s", err),
			})
			return
		}
		var instanceData rapiInstancesBulk
		json.Unmarshal([]byte(content), &instanceData)

		c.JSON(200, gin.H{
			"cluster":           name,
			"numberOfInstances": len(instanceData),
			"instances":         instanceData,
		})
	}
}

func getSingleInstanceHandler(c *gin.Context) {
	name := c.Param("cluster")
	instanceName := c.Param("instance")
	if !isValidCluster(name) {
		c.JSON(404, gin.H{"code": "PAGE_NOT_FOUND", "message": "Cluster not found"})
	} else {
		content, err := rapiGet(name, "/2/instances/"+instanceName)
		if err != nil {
			c.JSON(502, gin.H{
				"code":    "INTERNAL_SERVER_ERROR",
				"message": fmt.Sprintf("RAPI Backend Error: %s", err),
			})
			return
		}
		var instanceData rapiInstance
		json.Unmarshal([]byte(content), &instanceData)

		c.JSON(200, gin.H{
			"cluster":  name,
			"instance": instanceData,
		})
	}
}

func getInstanceConsoleHandler(c *gin.Context) {
	name := c.Param("cluster")
	instanceName := c.Param("instance")
	if !isValidCluster(name) {
		c.JSON(404, gin.H{"code": "PAGE_NOT_FOUND", "message": "Cluster not found"})
	} else {
		content, err := rapiGet(name, "/2/instances/"+instanceName)
		if err != nil {
			log.Errorf("RAPI Backend Error: %s", err)
			c.JSON(502, gin.H{
				"code":    "INTERNAL_SERVER_ERROR",
				"message": fmt.Sprintf("RAPI Backend Error: %s", err),
			})
			return
		}
		var instanceData rapiInstance
		err = json.Unmarshal([]byte(content), &instanceData)
		if err != nil {
			log.Errorf("Could not parse JSON result into RapiInstance struct: %s", err)
			c.JSON(502, gin.H{
				"code":    "INTERNAL_SERVER_ERROR",
				"message": fmt.Sprintf("RAPI Backend Error: %s", err),
			})
			return
		}
		// overwrite host/port for the websocket proxy "backend" for debugging:
		//instanceData.Pnode = "127.0.0.1"
		//instanceData.NetworkPort = 3333
		if instanceData.OperState && instanceData.NetworkPort > 0 {
			err = websocketHandler(c.Writer, c.Request, instanceData.Pnode, instanceData.NetworkPort)
			return
		}
		log.Infof("Cannot request console for shutdown instance '%s'", instanceData.Name)
		c.JSON(502, gin.H{
			"code":    "BAD_REQUEST",
			"message": fmt.Sprintf("Cannot request console for shutdown instance '%s'", instanceData.Name),
		})
		return
	}
}

func createSingleInstanceHandler(c *gin.Context) {
	name := c.Param("cluster")
	if !isValidCluster(name) {
		c.JSON(404, gin.H{"code": "PAGE_NOT_FOUND", "message": "Cluster not found"})
	} else {
		var instanceParameters createInstanceParameters
		c.BindJSON(&instanceParameters)
		_, err := rapiGet(name, "/2/instances/"+instanceParameters.InstanceName)
		if err == nil {
			c.JSON(400, gin.H{
				"code":    "RESOURCE_ALREADY_EXISTS",
				"message": fmt.Sprintf("Cannot create instance '%s', it already exists", instanceParameters.InstanceName),
			})
			return
		}
		newInstance := newGanetiInstance(instanceParameters)

		status, err := rapiPost(name, "/2/instances", newInstance)
		if err != nil {
			c.JSON(502, gin.H{
				"code":    "INTERNAL_SERVER_ERROR",
				"message": fmt.Sprintf("RAPI Backend Error: %s", err),
			})
			return
		}
		gntJobID, err := strconv.Atoi(strings.TrimSpace(status))
		if err != nil {
			c.JSON(502, gin.H{
				"code":    "INTERNAL_SERVER_ERROR",
				"message": fmt.Sprintf("Could not parse/understand RAPI JobID: %s", err),
			})
		} else {
			c.JSON(200, gin.H{
				"cluster":  name,
				"instance": newInstance.InstanceName,
				"status":   "jobSubmitted",
				"jobId":    gntJobID,
			})
		}
		return

	}
}

func createMultipleInstancesHandler(c *gin.Context) {

}

func getJobsHandler(c *gin.Context) {
	name := c.Param("cluster")
	if !isValidCluster(name) {
		c.JSON(404, gin.H{"code": "PAGE_NOT_FOUND", "message": "Cluster not found"})
	} else {
		content, err := rapiGet(name, "/2/jobs?bulk=1")
		if err != nil {
			c.JSON(502, gin.H{
				"code":    "INTERNAL_SERVER_ERROR",
				"message": fmt.Sprintf("RAPI Backend Error: %s", err),
			})
			return
		}
		var jobsData rapiJobsBulk
		json.Unmarshal([]byte(content), &jobsData)

		var jobsCount rapiJobStatusCount

		for _, job := range jobsData {
			switch job.Status {
			case "canceled":
				jobsCount.Canceled++
			case "error":
				jobsCount.Error++
			case "pending":
				jobsCount.Pending++
			case "queued":
				jobsCount.Queued++
			case "success":
				jobsCount.Success++
			case "waiting":
				jobsCount.Waiting++
			}
		}
		c.JSON(200, gin.H{
			"cluster":              name,
			"numberOfJobs":         len(jobsData),
			"numberOfJobsByStatus": jobsCount,
			"jobs":                 jobsData,
		})
	}
}

func getSingleJobHandler(c *gin.Context) {
	name := c.Param("cluster")
	jobID := c.Param("jobId")

	waitForFinish := false
	q := c.Request.URL.Query()
	blockParam, found := q["block"]
	if found {
		blockVal, err := strconv.ParseBool(blockParam[0])
		if err == nil && blockVal {
			waitForFinish = true
		}
	}

	if !isValidCluster(name) {
		c.JSON(404, gin.H{"code": "PAGE_NOT_FOUND", "message": "Cluster not found"})
	} else {
		content, err := rapiGet(name, "/2/jobs/"+jobID)
		if err != nil {
			c.JSON(502, gin.H{
				"code":    "INTERNAL_SERVER_ERROR",
				"message": fmt.Sprintf("RAPI Backend Error: %s", err),
			})
			return
		}
		var jobData rapiJob
		json.Unmarshal([]byte(content), &jobData)
		if waitForFinish {
			sanityCounter := 0
			for range time.Tick(time.Second) {
				if sanityCounter > 300 {
					c.JSON(502, gin.H{
						"code":    "INTERNAL_SERVER_ERROR",
						"message": fmt.Sprintf("Timed out waiting for jobId %s to finish/fail (300 seconds).", jobID),
					})
					return
				} else if jobData.Status != "success" && jobData.Status != "finished" {
					content, err := rapiGet(name, "/2/jobs/"+jobID)
					if err != nil {
						c.JSON(502, gin.H{
							"code":    "INTERNAL_SERVER_ERROR",
							"message": fmt.Sprintf("RAPI Backend Error: %s", err),
						})
						return
					}
					json.Unmarshal([]byte(content), &jobData)
					sanityCounter++
				} else {
					break
				}
			}
		}
		c.JSON(200, gin.H{
			"cluster": name,
			"job":     jobData,
		})
	}
}
