package main

import (
	"fmt"
	"time"

	jwt "github.com/appleboy/gin-jwt"
	"github.com/gin-gonic/gin"
	"github.com/jtblin/go-ldap-client"
	log "github.com/sirupsen/logrus"
)

type login struct {
	Username string `form:"username" json:"username" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
}

type user struct {
	UserName  string
	FirstName string
	LastName  string
}

func validateUser(userID string, password string) bool {
	switch C.AuthenticationMethod {
	case "builtin":
		return validateLocalUser(userID, password)
	case "ldap":
		return validateLdapUser(userID, password)
	}
	return false
}

func validateLocalUser(userID string, password string) bool {
	for _, tuple := range C.Users {
		if tuple.Username == userID && tuple.Password == password {
			return true
		}
	}
	return false
}

func validateLdapUser(userID string, password string) bool {
	client := &ldap.LDAPClient{
		Base:               C.LDAPConfig.BaseDN,
		Host:               C.LDAPConfig.Host,
		ServerName:         C.LDAPConfig.Host,
		Port:               C.LDAPConfig.Port,
		InsecureSkipVerify: C.LDAPConfig.SkipCertificateVerify,
		UserFilter:         C.LDAPConfig.UserFilter,
		GroupFilter:        C.LDAPConfig.GroupFilter,
	}
	defer client.Close()
	ok, _, err := client.Authenticate(userID, password)
	if err != nil {
		log.Errorf("Error authenticating user '%s': %+v", userID, err)
		return false
	}
	if !ok {
		log.Warningf("Authenticating failed for user '%s'", userID)
	}

	groups, err := client.GetGroupsOfUser(userID)
	if err != nil {
		log.Errorf("Error getting groups for user %s: %+v", "username", err)
	}

	if len(groups) > 0 {
		log.Debugf("Authentication for user '%s' successful", userID)
		return true
	}

	log.Warningf("User '%s' does not belong to LDAP groups matching the search filter", userID)
	return false
}

func getAuthMiddleware() (authMiddleware *jwt.GinJWTMiddleware) {
	// the jwt middleware
	timeout, err := time.ParseDuration(C.JwtExpire)
	if err != nil {
		panic(fmt.Errorf("Could not parse time duration format %s", err))
	}
	authMiddleware, err = jwt.New(&jwt.GinJWTMiddleware{
		Realm:       "gnt-cc",
		Key:         []byte(C.JwtSigningKey),
		Timeout:     timeout,
		MaxRefresh:  time.Hour,
		IdentityKey: identityKey,
		PayloadFunc: func(data interface{}) jwt.MapClaims {
			if v, ok := data.(*user); ok {
				return jwt.MapClaims{
					identityKey: v.UserName,
				}
			}
			return jwt.MapClaims{}
		},
		IdentityHandler: func(c *gin.Context) interface{} {
			claims := jwt.ExtractClaims(c)
			return &user{
				UserName: claims["id"].(string),
			}
		},
		Authenticator: func(c *gin.Context) (interface{}, error) {
			var loginVals login
			if err := c.ShouldBind(&loginVals); err != nil {
				return "", jwt.ErrMissingLoginValues
			}
			userID := loginVals.Username
			password := loginVals.Password

			if validateUser(userID, password) {
				return &user{
					UserName:  userID,
					LastName:  "Not provided",
					FirstName: "Not Provided",
				}, nil
			}
			return nil, jwt.ErrFailedAuthentication
		},
		Authorizator: func(data interface{}, c *gin.Context) bool {
			// for now we just validate if the data is valid User struct
			if _, ok := data.(*user); ok {
				return true
			}

			return false
		},
		Unauthorized: func(c *gin.Context, code int, message string) {
			c.JSON(code, gin.H{
				"code":    code,
				"message": message,
			})
		},
		// TokenLookup is a string in the form of "<source>:<name>" that is used
		// to extract token from the request.
		// Optional. Default value "header:Authorization".
		// Possible values:
		// - "header:<name>"
		// - "query:<name>"
		// - "cookie:<name>"
		// - "param:<name>"
		TokenLookup: "header: Authorization, query: token, cookie: jwt, param: token",
		// TokenLookup: "query:token",
		// TokenLookup: "cookie:token",

		// TokenHeadName is a string in the header. Default value is "Bearer"
		TokenHeadName: "Bearer",

		// TimeFunc provides the current time. You can override it to use another time value. This is useful for testing or if your server uses a different time zone than your tokens.
		TimeFunc: time.Now,
	})

	if err != nil {
		log.Errorf("JWT Error:" + err.Error())
	}

	return
}
