package main

import (
	"crypto/tls"
	"fmt"
	"net"
	"net/http"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

func validateConfig() bool {
	if !isInSlice(C.AuthenticationMethod, validAuthMethods) {
		panic(fmt.Sprintf("'%s' is not a valid Authentication Method (available methods: %v)", C.AuthenticationMethod, validAuthMethods))
	}

	switch C.AuthenticationMethod {
	case "builtin":
		if len(C.Users) == 0 {
			panic(fmt.Sprintf("Authentication Method has been set to 'builtin' but no users have been specified."))
		}
	}

	if len(C.Clusters) == 0 {
		panic(fmt.Sprintf("No Ganeti clusters have been specified in the configuration file."))
	}

	return true
}

func configParser() {
	viper.SetConfigName("config")
	viper.AddConfigPath(".")

	viper.SetDefault("bind", "127.0.0.1")
	viper.SetDefault("port", "8000")
	viper.SetDefault("developmentMode", "false")
	viper.SetDefault("jwtExpire", "1m")
	viper.SetDefault("AuthenticationMethod", "builtin")
	viper.SetDefault("logLevel", "warning")

	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s", err))
	}

	err = viper.Unmarshal(&C)

	if err != nil {
		panic(fmt.Errorf("unable to decode into struct, %v", err))
	}

	log.SetLevel(parseLogLevel(C.Loglevel))
	log.Debugf("Current configuration:\n%+v\n", C)

	validateConfig()
}

func parseLogLevel(logLevel string) log.Level {
	switch strings.ToLower(logLevel) {
	case "debug":
		return log.DebugLevel
	case "info":
		return log.InfoLevel
	case "warning":
		return log.WarnLevel
	case "error":
		return log.ErrorLevel
	case "fatal":
		return log.FatalLevel
	}
	log.Fatalf("Invalid loglevel given: %s", logLevel)
	return log.WarnLevel
}

func isInSlice(needle string, list []string) bool {
	for _, entry := range list {
		if entry == needle {
			return true
		}
	}
	return false
}

func isValidCluster(clusterName string) bool {
	for _, cluster := range C.Clusters {
		if cluster.Name == clusterName {
			return true
		}
	}
	return false
}

func getClusterConfig(clusterName string) ganetiCluster {
	for _, cluster := range C.Clusters {
		if cluster.Name == clusterName {
			return cluster
		}
	}
	panic(fmt.Sprintf("Could not find requested config for ganeti cluster '%s'", clusterName))
}

func getRapiConnection(clusterName string) (string, *http.Client) {
	cluster := getClusterConfig(clusterName)
	var url string
	if cluster.SSL {
		url = "https://"
	} else {
		url = "http://"
	}
	url = url + fmt.Sprintf("%s:%s@%s:%d", cluster.Username, cluster.Password, cluster.Hostname, cluster.Port)

	var tr = &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		Dial: (&net.Dialer{
			Timeout: 5 * time.Second,
		}).Dial,
		TLSHandshakeTimeout: 5 * time.Second,
	}

	var netClient = &http.Client{
		Timeout:   time.Second * 10,
		Transport: tr,
	}

	return url, netClient
}

func newGanetiInstance(instanceDetails createInstanceParameters) rapiInstanceCreate {
	var inst rapiInstanceCreate
	inst.InstanceName = instanceDetails.InstanceName
	inst.DiskTemplate = instanceDetails.DiskTemplate

	if len(instanceDetails.Nics) == 0 {
		inst.Nics = make([]ganetiNic, 0)
	} else {
		for _, nic := range instanceDetails.Nics {
			inst.Nics = append(inst.Nics, nic)
		}
	}

	if len(instanceDetails.Disks) == 0 {
		inst.Disks = make([]ganetiDisk, 0)
	} else {
		for _, disk := range instanceDetails.Disks {
			inst.Disks = append(inst.Disks, disk)
		}
	}

	if instanceDetails.Vcpus > 0 {
		inst.BeParams.Vcpus = instanceDetails.Vcpus
	}

	if instanceDetails.MemoryInMegabytes > 0 {
		inst.BeParams.Memory = instanceDetails.MemoryInMegabytes
	}

	inst.Version = 1
	inst.Mode = "create"
	inst.Hypervisor = "fake"
	inst.Iallocator = "hail"
	inst.OsType = "noop"
	inst.ConflictsCheck = false
	inst.IPCheck = false
	inst.NameCheck = false
	inst.NoInstall = true
	inst.WaitForSync = false
	return inst
}
