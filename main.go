package main

import (
	"net/http"
	"strconv"
	"time"

	jwt "github.com/appleboy/gin-jwt"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

var identityKey = "id"

type config struct {
	Bind                 string
	Port                 int
	DevelopmentMode      bool
	JwtSigningKey        string
	JwtExpire            string
	AuthenticationMethod string
	Loglevel             string
	Users                []userSet
	Clusters             []ganetiCluster
	LDAPConfig           ldapConfig
}

type ganetiCluster struct {
	Name        string `mapstructure:"name"`
	Hostname    string `mapstructure:"hostname"`
	Port        int
	Description string
	Username    string
	Password    string
	SSL         bool
}

type userSet struct {
	Username string
	Password string
}

type ldapConfig struct {
	Host                  string
	Port                  int
	SkipCertificateVerify bool
	BaseDN                string
	UserFilter            string
	GroupFilter           string
}

var validAuthMethods = []string{
	"builtin",
	"ldap",
}

// C is the global configuration variable
var C config

func main() {

	configParser()

	if !C.DevelopmentMode {
		gin.SetMode(gin.ReleaseMode)
	}
	r := gin.New()
	r.Use(gin.Logger())
	r.Use(gin.Recovery())

	if C.DevelopmentMode {
		r.Use(cors.New(cors.Config{
			AllowOrigins:     []string{"*", "http://localhost:8080"},
			AllowCredentials: true,
			AllowWebSockets:  true,
			AllowHeaders:     []string{"Origin", "Content-Length", "Content-Type", "Authorization"},
			MaxAge:           12 * time.Hour,
		}))
	}

	authMiddleware := getAuthMiddleware()

	r.POST("/v1/login", authMiddleware.LoginHandler)

	r.NoRoute(authMiddleware.MiddlewareFunc(), func(c *gin.Context) {
		claims := jwt.ExtractClaims(c)
		log.Warningf("NoRoute claims: %#v\n", claims)
		c.JSON(404, gin.H{"code": "PAGE_NOT_FOUND", "message": "Page not found"})
	})

	auth := r.Group("/v1")
	// Refresh time can be longer than token timeout
	auth.GET("/refresh_token", authMiddleware.RefreshHandler)
	auth.Use(authMiddleware.MiddlewareFunc())
	{
		auth.GET("/clusters", getclustersHandler)
		auth.GET("/clusters/:cluster", getClusterHandler)
		auth.GET("/clusters/:cluster/nodes", getNodesHandler)
		auth.GET("/clusters/:cluster/instances", getInstancesHandler)
		auth.GET("/clusters/:cluster/instance/:instance", getSingleInstanceHandler)
		auth.POST("/clusters/:cluster/instance", createSingleInstanceHandler)
		auth.POST("/clusters/:cluster/instances", createMultipleInstancesHandler)
		auth.GET("/clusters/:cluster/console/:instance", getInstanceConsoleHandler)
		auth.GET("/clusters/:cluster/jobs", getJobsHandler)
		auth.GET("/clusters/:cluster/job/:jobId", getSingleJobHandler)
	}

	bindInfo := C.Bind + ":" + strconv.Itoa(C.Port)
	log.Infof("Starting HTTP server on %s", bindInfo)
	if err := http.ListenAndServe(bindInfo, r); err != nil {
		log.Fatal(err)
	}

}
