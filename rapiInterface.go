package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"

	log "github.com/sirupsen/logrus"
)

func rapiGet(clusterName string, resource string) (string, error) {
	url, netClient := getRapiConnection(clusterName)

	log.Infof("RAPI GET %s", resource)
	response, err := netClient.Get(url + resource)
	if err != nil {
		log.Errorf("HTTP RAPI Connect: %s", err)
		return "", err
	}

	if response.StatusCode != 200 {
		log.Errorf("HTTP RAPI Bad Status Code: %d", response.StatusCode)
		return "", fmt.Errorf("Bad RAPI Status Code: %d", response.StatusCode)
	}
	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Errorf("HTTP RAPI Reading Body: %s", err)
		return "", err
	}

	return string(body), err
}

func rapiPost(clusterName string, resource string, postData interface{}) (string, error) {
	url, netClient := getRapiConnection(clusterName)

	jsonData, err := json.Marshal(postData)
	if err != nil {
		return "", fmt.Errorf("Could not prepare JSON for RAPI Request: %s", err)
	}

	log.Infof("RAPI POST %s", resource)
	response, err := netClient.Post(url+resource, "application/json", bytes.NewBuffer(jsonData))
	if err != nil {
		log.Errorf("HTTP RAPI Connect: %s", err)
		return "", err
	}
	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Errorf("HTTP RAPI Reading Body: %s", err)
		return "", err
	}

	if response.StatusCode != 200 {
		log.Errorf("HTTP RAPI Bad Status Code: %d", response.StatusCode)
		log.Errorf("Answer: %s", body)
		return "", fmt.Errorf("Bad RAPI Status Code: %d", response.StatusCode)
	}

	return string(body), err
}
