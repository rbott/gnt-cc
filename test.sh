#!/bin/bash -e

RET_TOKEN=`curl -s -X POST -d '{"username":"admin","password":"admin"}' localhost:8000/v1/login -H "Content-Type: application/json"`
echo $RET_TOKEN | jq .
TOKEN=`echo $RET_TOKEN| jq -r ".token"`

echo
echo "Querying list of available clusters:"

RET_CLUSTERS=`curl -s -X GET localhost:8000/v1/clusters -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN"`
echo $RET_CLUSTERS | jq .
CLUSTER=`echo $RET_CLUSTERS | jq -r ".clusters[0]"`

echo
echo "Querying details of cluster one:"

RET_CLUSTER=`curl -s -X GET localhost:8000/v1/clusters/$CLUSTER -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN"`
echo $RET_CLUSTER | jq .

echo
echo "Querying nodes of cluster one:"

RET_NODES=`curl -s -X GET localhost:8000/v1/clusters/$CLUSTER/nodes -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN"`
echo $RET_NODES | jq .

echo
echo "Querying instances of cluster one:"

RET_INSTANCES=`curl -s -X GET localhost:8000/v1/clusters/$CLUSTER/instances -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN"`
echo $RET_INSTANCES | jq .
INSTANCE=`echo $RET_INSTANCES | jq -r '.instances[0].name'`

echo
echo "Querying instance one:"

RET_INSTANCE=`curl -s -X GET localhost:8000/v1/clusters/$CLUSTER/instance/$INSTANCE -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN"`
echo $RET_INSTANCE | jq .

#echo
#echo "Querying console for instance one (press Ctrl+C to cancel websocket client test):"
#../websocket-echo/websocket-echo ${TOKEN}

echo
echo "Querying jobs of cluster one:"

RET_JOBS=`curl -s -X GET localhost:8000/v1/clusters/$CLUSTER/jobs -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN"`
echo $RET_JOBS | jq .
JOB=`echo $RET_JOBS | jq -r '.jobs[0].id'`

echo
echo "Querying job one:"

RET_JOB=`curl -s -X GET localhost:8000/v1/clusters/$CLUSTER/job/$JOB -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN"`
echo $RET_JOB | jq .


echo
echo "Creating already existing Instance:"
RET_INST_CREATE=`curl -s -X POST localhost:8000/v1/clusters/$CLUSTER/instance -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" \
  -d "{ \"instance_name\": \"bart\", \"disk_template\": \"diskless\", \"disks\": [ { \"size\": 100} ], \"nics\": [ { \"link\" : \"br100\" } ] }"`
echo $RET_INST_CREATE | jq .

echo
echo "Creating Instance:"


RET_INST_CREATE=`curl -s -X POST localhost:8000/v1/clusters/$CLUSTER/instance -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" \
  -d "{ \"instance_name\": \"apu\", \"disk_template\": \"diskless\", \"vcpus\": 4, \"memoryInMegabytes\": 256 }"`
echo $RET_INST_CREATE | jq .
JOB=`echo $RET_INST_CREATE | jq .jobId`

echo 
echo "Waiting for Create-Instance Job to finish"
RET_JOB=`curl -s -X GET localhost:8000/v1/clusters/$CLUSTER/job/$JOB?block=true -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN"`
echo $RET_JOB | jq .

